change log:
20180613 - 0.0.17:
1. Fixed camera issue, version < 7.0.0
2. Fixed click issue in tools module
3. Updated end point success message
4. Updated Datepicker Calendar View to Datepicker Spinner View

change log:
20180323 - 0.0.16:

1. spelling correction trail to trial
2. set mobile text to be disable on receiving otp


change log:
20180320 - 0.0.15:
1. Register Page change button name payment to submit
2. Payment Page change button name payment to submit
3. In Payment Page add one option Free Trail
4. When select Free Trail amount should be zero ,no need to go through payment gateway and go to Registration Success API


change log:
20180314 - 0.0.13:

1. fixed registration payment amount rounding to two decimals
2. fixed doctor advice null data in date & message
3. fixed BMI and BFP success message
4. fixed formatting for cgst
5. solved release errors related to saripaar

change log:
20180314 - 0.0.12:

1. added authorization to change password endpoint
2. added gstin, panno, gst, amount to invoice
3. hide Family Members menu link for single mode

change log:
20180313 - 0.0.11:
1. fixed signup validating on invalid otp
2. disabled edittexts in invoice screen
3. removed payumoney flow for 0 amount
4. added explicit self dropdown if Profile Mode
5. hide Family Members for single registration

change log:
20180217 - 0.0.10:

1. Added share to immunization detail screen
2. [C1HAA128] added immunization to sharable
3. Added padding to HBA1C
4. [C1HAA108,C1HAA132] Disable mobile on click otp. Set Aadhar to non-mandatory
5. [C1HAA134] Added refresh list on click of Show Records link
6. [C1HAA133] update string error response

change log:
20180216 - 0.0.9:
1. incorrect deployment

change log:
20180216 - 0.0.8:
1. payment gateway integration issue resolved

change log:
20180215 - 0.0.7:
1. Enhancements - date UTC, OAuth2, new API endpoint and fields update
2. Extracted all strings to strings.xml to make ready for i18ln

20180215 - TODO:
1. [x] payment gateway integration

change log:
20180109 - 0.0.6:
1. change name of the signUp screen 
2. change hint in promoCode field while payment process
3. On save of new member app incorrectly adding same member multiple times
4. Add change password dialog
5. [C1HAA84] restricted new lab test rows results to numeric only 
6. [C1HAA88] Forced refresh on screen only on save
7. [C1HAA87] Added screen titles to quick share, set reminder, invoice, family member
8. [C1HAA78] Updated validation for all text box to begin with alpha numeric
9. [C1HAA86] Validation options added for Manual Entry and Upload FIle in Lab Test/X-Ray/Prescription/Discharge summary/Insurance Documents/Vision
10. [C1HAA81] OTP added to Profile screen
11. [C1HAA30] added alert notification to bottom navigation view

20180109 - TODO:
1. [x] change name of the signUp screen 
2. [x] change hint in promoCode field while payment process
3. [x] On save of new member app incorrectly adding same member multiple times
4. [x] Add change password dialog
 

20180102 - 0.0.5:
1. removed default dummy data population as per clients request

20180102 - 0.0.4:
1. [C1HAA41] Sorted menu item as per clients request
2. [C1HAA51] Feature to disallow future and past dates
3. Refactored new logs dialog actions based on user selection
4. [C1HAA65] Added Atleast one value should be entered for new vision row
5. [C1HAA68] Updated new Id for Immunization from null to new

20180102 - TODO:


20180102 - 0.0.3:
1. Added Scroll view to Lab Test New Row Dialog
2. Added Quick Share Functionality
3. Enabled History for using Back button
4. Added Member selection to Reminder List
5. Automatic Refresh List on update of detail
6. Refresh Dashboard on First Login
7. Speed Optimizations ##
8. Added Prompt if network not available
9. Added Sorting to BP and BS dates in Dashboard
10. Fixed - Prescription Dialog not displaying Tine and Instruction in row edit mode
11. Fixed error - on click screen name, new dialog is opening is Reminder Edit mode
12. Maintained snackbar for Error and toast for message
13. Added MandatoryCheckRule to I Agree checkbox
14. Add Sign Up Functionality

20180102 - TODO:
1. [x] Keeping Toast layout consistent across app as in DOB field
2. [x] App icon pixillating in >M
3. [x] Seekbar not resetting on save in >M
4. [x] Prescription Dialog not displaying Tine and Instruction in row edit mode
5. [x] On click screen name, new dialog is opening is Reminder Edit mode
6. [x] Prompt if network not available


20171227 - 0.0.2:
1. Added Forgot Password
2. Fixed issues with Lab Test List details
3. Fixed issues mentioned in Defect Report 19-12-2017

20171227 - TODO:
1. [x] Add Sign Up Functionality
2. [x] Add Quick Share Functionality
3. [x] Enable History for using Back button
4. [x] Automatic Refresh List on update of detail
5. [x] Refresh Dashboard on First Login
6. [x] Add Notification of New alert (need to discuss with client)